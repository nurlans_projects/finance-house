import { SendemailService } from './../sendemail.service';
import { EmailData } from './../email-data';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  data = new EmailData('', '',null, '',   '')
  constructor(private _sendEmail: SendemailService ) { }

  ngOnInit() {
  }

  onSubmit(){
    this._sendEmail.send(this.data)
      .subscribe(
        data => console.log('Success!', data),
        error => console.error('Error!!!', error)

      )
      
  }
}

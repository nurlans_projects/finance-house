import { EmailData } from './email-data';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class SendemailService {
  _url = 'http://127.0.0.1:8000/api/emails';
  constructor(private _http: HttpClient) { }

  send(data: EmailData){
    return this._http.post<any>(this._url, data);
  }
}

import { LanguagesService } from './languages.service';
import { NewsService } from './news.service';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CarouselComponent } from './carousel/carousel.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { NewsComponent } from './news/news.component';
import { ContactComponent } from './contact/contact.component';
import { NavbarModule, WavesModule, ButtonsModule ,InputsModule, InputUtilitiesModule, } from 'angular-bootstrap-md';

import { ContentComponent } from './content/content.component';
import { FinanceServicesComponent } from './finance-services/finance-services.component';
import { AboutUsDetailComponent } from './about-us-detail/about-us-detail.component';
import { NewsDetailComponent } from './news-detail/news-detail.component'

const appRoutes: Routes = [
  { path: 'home', component: AppComponent },
  { path: 'about', component: AboutUsComponent },
  { path: 'detail', component: AboutUsDetailComponent },
  { path: 'service', component: AboutUsComponent },
  { path: 'news', component: NewsComponent },
  { path: 'news/:id', component: NewsDetailComponent},
  { path: 'contact', component: ContactComponent },
  
  { path: '**', component: AppComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    CarouselComponent,
    AboutUsComponent,
    NewsComponent,
    ContactComponent,
    ContentComponent,
    FinanceServicesComponent,
    AboutUsDetailComponent,
    NewsDetailComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    NavbarModule, 
    WavesModule,
    ButtonsModule,
    InputsModule,
    InputUtilitiesModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
    
  ],
  providers: [NewsService, LanguagesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
    
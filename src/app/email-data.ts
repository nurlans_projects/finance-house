export class EmailData {
    constructor(
        public name: string,
        public email: string,
        public phone: number,
        public title: string,
        public text: string,
    ){}
}

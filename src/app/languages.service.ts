import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class LanguagesService {
  constructor() {}

  getAzLang() {
    return [
      {
        "aboutUs": "Haqqımızda",
        "services": "Xidmətlər",
        "contact": "Əlaqə",
        "news": "Xəbərlər"
      }
    ];
  }
  getRuLang() {
    return [
      {
        "aboutUs": "О нас",
        "services": "Услуги",
        "contact": "Контакты",
        "news": "Новости"
      }
    ];
  }
  getEnLang() {
    return [
      {
        "aboutUs": "About us",
        "services": "Services",
        "contact": "Contacts",
        "news": "News"
      }
    ];
  }
}

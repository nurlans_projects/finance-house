import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
 
@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html',
  styleUrls: ['./news-detail.component.scss']
})
export class NewsDetailComponent implements OnInit {
  public newsId;
  constructor(private actRoute: ActivatedRoute) { }

  ngOnInit() {
    var id = parseInt(this.actRoute.snapshot.paramMap.get('id'));
    
    this.newsId = id;
    console.log(this.actRoute.snapshot.paramMap, "LALA")
     

  }

}

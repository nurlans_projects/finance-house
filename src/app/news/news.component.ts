import { NewsService } from './../news.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'


@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  public news = [];
  public errorMsg;
  constructor(private _newsService: NewsService, private router: Router) { }

  ngOnInit() {
    this._newsService.getNews()
      .subscribe(data => this.news = data,
      error => this.errorMsg = error
      );
      
  }
  
  isNewsFull(){
    return this.news.length > 0;
  }

  onSelect(n){
    this.router.navigate(['/news', n.id]);
  }
}

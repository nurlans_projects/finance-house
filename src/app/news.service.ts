import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { INews } from './news';

import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  private _url: string = "http://127.0.0.1:8000/news";


  constructor(private http: HttpClient) { }
  getNews(): Observable<INews[]>{
    return this.http.get<INews[]>(this._url)
                    .catch(this.errorHandler)
       
  }
  errorHandler(error: HttpErrorResponse){
      return Observable.throw(error.message || "Server Error")
  }
}

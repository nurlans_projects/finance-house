import { LanguagesService } from './../languages.service';
import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public langData =  [];

  constructor(private _langService: LanguagesService ) { }

  ngOnInit() {
    this.langData = this._langService.getAzLang();
  }
  
  dropAzClick(){
    this.langData = this._langService.getAzLang();
  }
  dropRuClick(){
    this.langData = this._langService.getRuLang();
  }
  dropEnClick(){
    this.langData = this._langService.getEnLang();
  }
  
}
